NOTE: Please use a markdown viewer with bright background color (required due to plot labels on transparent background) 
or view via <code>plots.html</code>.

# Chinanet
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_chinanet_kmeans.png "clusters")|![partitions](./topo/topo_chinanet_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_chinanet.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_chinanet.png "churn")

# Litnet
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_litnet_kmeans.png "clusters")|![partitions](./topo/topo_litnet_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_litnet.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_litnet.png "churn")

# Cernet
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_cernet_kmeans.png "clusters")|![partitions](./topo/topo_cernet_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_cernet.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_cernet.png "churn")

# Ntt
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_ntt_kmeans.png "clusters")|![partitions](./topo/topo_ntt_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_ntt.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_ntt.png "churn")

# Cesnet200706
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_cesnet200706_kmeans.png "clusters")|![partitions](./topo/topo_cesnet200706_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_cesnet200706.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_cesnet200706.png "churn")

# Carnet
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_carnet_kmeans.png "clusters")|![partitions](./topo/topo_carnet_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_carnet.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_carnet.png "churn")

# Dfn
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_dfn_kmeans.png "clusters")|![partitions](./topo/topo_dfn_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_dfn.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_dfn.png "churn")

# Telcove
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_telcove_kmeans.png "clusters")|![partitions](./topo/topo_telcove_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_telcove.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_telcove.png "churn")

# Forthnet
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_forthnet_kmeans.png "clusters")|![partitions](./topo/topo_forthnet_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_forthnet.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_forthnet.png "churn")

# Bellsouth
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_bellsouth_kmeans.png "clusters")|![partitions](./topo/topo_bellsouth_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_bellsouth.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_bellsouth.png "churn")

# Garr200902
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_garr200902_kmeans.png "clusters")|![partitions](./topo/topo_garr200902_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_garr200902.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_garr200902.png "churn")

# Arnes
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_arnes_kmeans.png "clusters")|![partitions](./topo/topo_arnes_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_arnes.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_arnes.png "churn")

# BeyondTheNetwork
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_beyondthenetwork_kmeans.png "clusters")|![partitions](./topo/topo_beyondthenetwork_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_beyondthenetwork.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_beyondthenetwork.png "churn")

# Uunet
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_uunet_kmeans.png "clusters")|![partitions](./topo/topo_uunet_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_uunet.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_uunet.png "churn")

# Tw
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_tw_kmeans.png "clusters")|![partitions](./topo/topo_tw_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_tw.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_tw.png "churn")

# Uninett
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_uninett_kmeans.png "clusters")|![partitions](./topo/topo_uninett_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_uninett.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_uninett.png "churn")

# Renater2010
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_renater2010_kmeans.png "clusters")|![partitions](./topo/topo_renater2010_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_renater2010.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_renater2010.png "churn")

# Surfnet
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_surfnet_kmeans.png "clusters")|![partitions](./topo/topo_surfnet_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_surfnet.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_surfnet.png "churn")

# Iris
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_iris_kmeans.png "clusters")|![partitions](./topo/topo_iris_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_iris.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_iris.png "churn")

# Palmetto
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_palmetto_kmeans.png "clusters")|![partitions](./topo/topo_palmetto_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_palmetto.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_palmetto.png "churn")

# BtLatinAmerica
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_btlatinamerica_kmeans.png "clusters")|![partitions](./topo/topo_btlatinamerica_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_btlatinamerica.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_btlatinamerica.png "churn")

# Bellcanada
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_bellcanada_kmeans.png "clusters")|![partitions](./topo/topo_bellcanada_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_bellcanada.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_bellcanada.png "churn")

# Sanet
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_sanet_kmeans.png "clusters")|![partitions](./topo/topo_sanet_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_sanet.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_sanet.png "churn")

# LambdaNet
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_lambdanet_kmeans.png "clusters")|![partitions](./topo/topo_lambdanet_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_lambdanet.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_lambdanet.png "churn")

# HiberniaGlobal
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_hiberniaglobal_kmeans.png "clusters")|![partitions](./topo/topo_hiberniaglobal_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_hiberniaglobal.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_hiberniaglobal.png "churn")

# Ntelos
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_ntelos_kmeans.png "clusters")|![partitions](./topo/topo_ntelos_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_ntelos.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_ntelos.png "churn")

# RedBestel
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_redbestel_kmeans.png "clusters")|![partitions](./topo/topo_redbestel_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_redbestel.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_redbestel.png "churn")

# VtlWavenet2008
## Clusters
|Groups from `clusters` strategy|Groups from `partitions` strategy|
|:---:|:---:|
|![clusters](./topo/topo_vtlwavenet2008_kmeans.png "clusters")|![partitions](./topo/topo_vtlwavenet2008_modularity.png "partitions")|
## Varying Number of Rules
![rules](./rules/rules_vtlwavenet2008.png "rules")
## Client Migration Scenarios
![churn](./churn/churn_vtlwavenet2008.png "churn")
