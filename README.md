# Virtual Delivery Trees Evaluation Results

Private repository for review, to be released on github and zenodo on publication.
View `README.md` with your favorite markdown viewer or `README.html` in browser.

The artifacts represent evaluation results of real world networks having more
than 40 nodes from [Network Topology Zoo](http://www.topology-zoo.org/). The
applied topologies are listed in following table, sorted in descending order
by diameter (d) and nodal degree fluctuation (σ^2)).

| Topology         | \|V\| | \|E\| | \<k\> | σ^2 | d |
|------------------|----|----|-----|-------|---|
| Chinanet         | 42 | 66 | 1.5 | 10.52 | 4 |
| Litnet           | 43 | 43 | 0.98 | 5.04 | 4 |
| Cernet           | 41 | 58 | 1.32 | 5.6 | 5 |
| Ntt              | 32 | 65 | 1.48 | 7.07 | 6 |
| Cesnet200706     | 44 | 51 | 1.16 | 6.27 | 6 |
| Carnet           | 44 | 43 | 0.98 | 5.48 | 6 |
| Dfn              | 50 | 78 | 1.77 | 5.31 | 6 |
| Telcove          | 71 | 70 | 1.59 | 9.13 | 7 |
| Forthnet         | 62 | 62 | 1.41 | 7.72 | 7 |
| Bellsouth        | 51 | 66 | 1.5 | 7.55 | 7 |
| Garr200902       | 54 | 68 | 1.55 | 5.13 | 7 |
| Arnes            | 41 | 57 | 1.3 | 4.53 | 7 |
| BeyondTheNetwork | 53 | 65 | 1.48 | 3.98 | 7 |
| Uunet            | 49 | 84 | 1.91 | 7.38 | 8 |
| Tw               | 71 | 115 | 2.61 | 5.58 | 8 |
| Uninett          | 71 | 97 | 2.2  | 3.12 | 9 |
| Renater2010      | 43 | 56 | 1.27 | 3.08 | 9 |
| Surfnet          | 50 | 68 | 1.55 | 3.36 | 11 |
| Iris             | 51 | 64 | 1.45 | 2.16 | 11 |
| Palmetto         | 45 | 64 | 1.45 | 2.57 | 12 |
| BtLatinAmerica   | 45 | 50 | 1.14 | 1.87 | 12 |
| Bellcanada       | 48 | 64 | 1.45 | 2.59 | 13 |
| Sanet            | 43 | 45 | 1.02 | 1.66 | 13 |
| LambdaNet        | 42 | 46 | 1.05 | 1.57 | 13 |
| HiberniaGlobal   | 55 | 81 | 1.84 | 2.72 | 16 |
| Ntelos           | 47 | 58 | 1.32 | 1.92 | 17 |
| RedBestel        | 84 | 93 | 2.11 | 0.85 | 28 |
| VtlWavenet2008   | 88 | 92 | 2.09 | 0.11 | 31 | 

The evaluation results consist of three major parts:
1. **Raw Data**: Configuration and results of all simulation experiments as CSV files.
2. **Strategy Results**: Visualization of the test results for each topology.
3. **Best Strategies**: Highlighting of the best strategies across all topologies.

Therein, the *Raw Data* comprise the configuration of or simulation
experiments and the simulation results. Each line stands for a single
simulation run.

*Strategy Results* and *Best Strategies* accompany the results presented in
the paper. Result plots in the paper are excerpts from the plots in this
repository. See below for further details.

# Raw Data 

Both, the configuration of a run and its results correspond to one line within
a CSV file in subfolder `./raw` . Each file comprises the results of a
replication. 

```
raw
├── results_0.csv
├── results_1.csv
├── ...
└── results_9.csv

```

The raw data of a CSV file is structured as follows.


| Column | Description |
|--------|-------------|
|topo| Topology name.
|peers| Number of nodes.|
|edges| Number of links.|
|p_publishers| Proportion of nodes acting as publisher (15% - 45%).|
|p_subscriber| Proportion of nodes acting as subscriber (15% - 45%).|
|n_rules| Number of allowed rules per switch.|
|distances| Flag for consideration of geographical distances (currently not used).|
|strategy| Applied virtual tree strategy.|
|distribution| Distribution method for client (uniform, distant, nearby)|
|n_cluster| Number of simulated clusters within the topology.|
|p_change| Churn rate of clients (0% - 100%). |
|pub_change| Flag for publisher migration (currently not used).|
|tree_count| Number of virtual trees installed in the network.|
|selected_subscribers|  Avg. number of subscribers addressed by a publisher|
|init_cost| Avg. number of entries of a non-optimized distribution tree (per notification)|
|trees| Avg. proportion of tree entries per notification.|
|stops| Avg. proportion of stop entries per notification.|
|hops| Avg. proportion of hop entries per notification.|
|final_cost| Aggregated proportions (trees + stops + hops).|
|datetime| Timestamp of the simulation run.|



# Result Charts

The simulation results are visualized in [plots.md](plots.md)
or [plots.html](plots.html), ordered according above topology table.

Each topology accompanys following:
- Toplogy figures with the computed **Clusters** therein.
- Line charts outlining the behavior of the strategies over changing **Number of Flow Rules**.
- Bar charts outlining the strategies' performance for different **Migration Scenarios**. 

Details of the figures and diagrams are described next.

## Clusters

Visualization of exemplary groups within the topology, computed by `clusters`
and `partitions` strategy. The clusters strategy assigns 60% of a network's
nodes to cluster groups; the partition strategy, in contrast, assigns all
nodes to groups. Both strategies are described in Sec. III.

## Number of Flow Rules

Results for varying number of rules (from 5 to 40) per switch, as described in
Sec. IV. The charts are organized in a 3 x 3 matrix. A row of the matrix
corresponds to different proportions of subscribers per publisher (15%, 30%,
and 45%); a column corresponds to different distributions of clients
(uniform, nearby and distant).
    
## Migration Scenarios

Results for different migration scenarios with a fixed number of rules
(40 rules per switch), as described in Sec. V. Each bar group stands for a
strategy and reflects the results of different migration rates (0%, 30%, 50%,
70%, 100%).

# Best Strategies

Scatter plots in subfolder `./fluctuation` visualize the most
efficient strategies for different migration scenarios by considering
different proportions of subscribers per publisher (15%, 30%, and 45%). The
plots show the results for a fixed number of subscribers (30% per publisher)
and a churn rate of 100%. The strategies therein require the fewest labels in
the header stack to encode a notification distribution tree, represented by
the strategy's dot size.

## 15% subscribers
![fluctuation_15](./fluctuation/fluctuation_15.png "15% subscribers")

## 30% subscribers
![fluctuation_30](./fluctuation/fluctuation_30.png "30% subscribers")

## 45% subscribers
![fluctuation_45](./fluctuation/fluctuation_45.png "45% subscribers")
